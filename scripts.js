$(document).on('click', 'someyourContainer .dropdown-menu', function (e) {
    e.stopPropagation();
});


function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

$(document).ready(function () {
    $('img').on('click', function () {
        var image = $(this).attr('src');
        $('#myModal').on('show.bs.modal', function () {
            $(".img-responsive").attr("src", image);

        });
    });
});

$(".navbar-toggle").on("click", function () {
    $(this).toggleClass("active");
});
